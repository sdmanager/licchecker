package licChecker

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/ProtonMail/gopenpgp/v2/helper"
	"github.com/panta/machineid"
	"gitlab.com/sdmanager/licchecker/internal/cryptoAES"
	"io"
	"net/http"
	"os"
	"os/user"
	"strings"
	"time"
)

const (
	// SDKVersion - The current version of the SDK.
	SDKVersion = "1.1.18"
)

type LicData struct {
	Exp          *int64 `json:"exp"`
	SerialNumber string `json:"sn"`
}

// New add model
//
//goland:noinspection GoUnusedExportedFunction
func New(product string) *Model {
	m := &Model{Product: product}
	return m
}

type Model struct {
	Product string
}

// Version returns a version package,
func (c *Model) Version() string {
	return SDKVersion
}

// Validate Checks key matching
func (c *Model) Validate(token string) error {

	protectedID, err := machineid.ProtectedID(c.Product)
	if err != nil {
		return fmt.Errorf("failed to get a machine key: %w", err)
	}

	lic, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		return fmt.Errorf("failed to decode license key: %w", err)
	}

	data, err := cryptoAES.KeyDecrypt(protectedID, string(lic[:]))
	if err != nil {
		return fmt.Errorf("failed to decrypt license key: %w", err)
	}

	var d LicData

	err = json.Unmarshal([]byte(data), &d)
	if err != nil {
		return fmt.Errorf("failed to decode: %w", err)
	}

	if d.Exp != nil && *d.Exp != 0 && time.Now().UnixMilli() >= *d.Exp {
		return fmt.Errorf("token expired")
	}

	if !c.checkStateWeb(token, d.SerialNumber) {
		return fmt.Errorf("token online state error")
	}
	return nil
}

// Key shows the key for registration
func (c *Model) Key() (string, error) {

	var t struct {
		Key      string `json:"key"`
		UserName string `json:"username"`
		UserGuid string `json:"projectId"`
		Username string `json:"userGuid"`
		HostName string `json:"hostname"`
	}
	protectedID, err := machineid.ProtectedID(c.Product)
	if err != nil {
		return "", fmt.Errorf("failed to get a machine key: %v", err)
	}
	t.Key = protectedID

	u, err := user.Current()
	if err == nil {
		t.UserName = u.Name
		t.UserGuid = u.Uid
		t.Username = u.Username
	}
	h, err := os.Hostname()
	if err == nil {
		t.HostName = h
	}
	b, err := json.Marshal(t)
	if err != nil {
		return "", fmt.Errorf("failed to marshal: %v", err)
	}
	d, err := helper.EncryptBinaryMessageArmored(publicKeyPGP, b)
	if err != nil {
		return "", fmt.Errorf("failed to encrypt: %v", err)
	}

	return base64.StdEncoding.EncodeToString([]byte(d)), nil

}
func (c *Model) checkStateWeb(token, serialNumber string) bool {
	var t struct {
		SN    string `json:"sn"`
		Token string `json:"token"`
	}
	t.SN = serialNumber
	t.Token = token

	b, err := json.Marshal(t)
	if err != nil {
		return false
	}
	d, err := helper.EncryptBinaryMessageArmored(publicKeyPGP, b)
	if err != nil {
		return false
	}

	req, err := http.NewRequest("POST", "https://lic.sdmanager.ru/api/v1/device/check", strings.NewReader(d))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	client.Timeout = time.Second
	resp, err := client.Do(req)
	if err != nil {
		return true
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)
	if resp.StatusCode == 600 {
		return false
	}
	return true
}

//goland:noinspection SpellCheckingInspection
const publicKeyPGP = `-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: Keybase OpenPGP v2.0.76

xsFNBGSeheEBEACw7/m+hnJuLkgE+s159Erk9ZXLabu0QBWAme8xTwV1+5Ybz0jp
ZS5Fu2H5kK0GPF1FKs+ByvVS1u3IuxapMCEN7tL2TLcydbhFQaDnOsBnDm3YC0pi
nL+ftq1JV1MpsHnTOtDQh36eVJtqjiWOKnuWU3ENBxBEWCpPaq2qx09fG5QY16c8
tEmhPHCcEI5F5qp2A9nd1pXdktlZs3iLiRJDAMjK4PdNiKOhOFAWX1YK+wKGu7VZ
RZKk5baGaEib/QoadFi82b0yAtwTkRF8Pa6Mj1devg6OiBh1HGoAyG14Vr5aWlMW
v8Ka+sn/8RasAG+Qa9IuaqhMB8eEswxXU5DCXWXz2EDx2T71557bDA6jUKECkQAF
FZrpoCiGXHocRolnee4xT4Cu/qRHg7o580Ww565tAULQu00c7RsqiUvxoRrLtGKa
jl3AQBccXnBkSYeZY8Bqs9svq5aPKkSDJ5FvOZAvJ6AP4n3f95EA+MPZN8MOccaN
Dlj4y9wAjaaCojPnBiBwIVAdjwdawu7o9FLA+WsHqQakFpJPPoc/eLUA+/l1SXku
NNRdCCzFYwalHcFFs2VuN7KqFbwEmhnJsWhlg02eTBJtbsnEN7xWkfyivVscV9Q4
5mAunXoEyeg2bXS2vPDKtQNtpPPWdoyaC+a4J1efpDb+7tYy9Q3POup3HQARAQAB
zRVtYWtzaW0gPG1peC15YUB5YS5ydT7CwXoEEwEKACQFAmSeheECGy8DCwkHAxUK
CAIeAQIXgAMWAgECGQEFCQAAAAAACgkQ1MHxg8t7nOagrQ//S6w2SAwUhGyuqank
kueXuWhgToNBXRR9iECJJW2NHjyiUwKhtzFEHE8oGeNXPw5Dk8x2dpqY9CMVPrSV
SqamfVEZyUvjib4IHlAVC08OmCtRqz8j+xoqv/ACPWCcuOeqXo3N/bxT+1v7k9Oc
0HFQB7FzMmsiRB33u13aw9KhO+LLHAr4L5hDZg1pxmTM070buN4crMcsF3sriV4N
3+xprdfm/RQ7R+j+CkNCcJAiv0M4A5+qmAodxj+Vkg64ZkdY461u6YfR/iJWuAQS
xKNKUMsf3L/UqMb9b1zT5BweOgQ6qrmbINRw/ISPILuSCzqwVApu5iW6S9J6yK4Z
GdclSVjqwJbn/Gj6r5aXPvhbWI3/1tI6Vf2eEEoSxIpflSCdi3XPvqRXd8+PL9Zd
lbw6X+UfvkdZDR8AQ/5XaniLlRjhEDtdDVwSb5o7ZCdmLJPnM4XyJNEnJq5rlpt3
w21azkgw4Nom78BTrugzwN6ofdfD+Vx52p2Co7qNOF79ccYhoSyb45zWpr1kaINl
NuHAPeVkgvT4SvOCItxdYu+GX4fECdJJ6WMD8hGEMgC1POqE5/tFV92mrHErBoo1
irq7t6GRXfdfcDxlISZ4iZQdEnrnSR3ok+Uafo/D2nLwpZfvWwfX/PG7nVD+yTFX
W19NTapt2v61tpv2LJRgV2MUABDOwU0EZJ6F4QEQALBaHBqG3uhgCDs99KGx+C+c
DsBt/B6oAz8BCihTt3I3OYqwSLfxJiv8d0j8FWSQ3XMdN6T9U4LDwzD9U8aRzUWY
i3ZPb4zxDZtvVjXwJhzkKlCy17AgTEDyleOMCqHURe2bObVTS2/9oIGXxDhAhIhc
NLmtlgEJVMJE6QJpnxl7GrR73DL9rp/cPeNCCIIeJodqun6V82ybf/uX0bE3Ox8F
v7kt+VQe4RgQC6LpU26ZmeFwHjlPjlFwzAPB0J0imU08+5N/EqVwFrgJQ3nm2L+7
kbxfD1S0pbyPD78Da+mU7ykppfs0myyoDHOqK02oCPjpGT8otn8E5R0EonZL4M/g
1iUReBNM0Pg2k+Y9DjYUHmCdRde3DFZrRME09aiTPxjaI1NOMoedTUng7ow+Fwf/
R5BefmbiVQYDxmMzGRJAgrK7yDTVpouiFyO0cpi1OYJqucgfsHHUng0fIIIy8Bor
OA1ZCDc4abfeATbwS1eSHZfrmXskzrvvgfW4EwKih8k5hnQ13FdUXolZW6S3pe42
tvDHR6GGHHlhVX6NJYTYArFWMMt7Wgp+o/hg4/Kmkqo3oRPVQMnze3NrzyVuNOh6
7DgrXxbkl8Vllt4UdxwS7JQUayq8quwQs9xjmD3wtEU+29vAxaiq+b8zqGhHn+1H
man0bOtqxFoc/3Zd8f2jABEBAAHCw4QEGAEKAA8FAmSeheEFCQAAAAACGy4CKQkQ
1MHxg8t7nObBXSAEGQEKAAYFAmSeheEACgkQwH3/GMf3xEck+w/+LC7j9JhQtO6d
alDo0KrOF7kMvbpfp9R5H7mdLC4jqAZ56jLg+rJbKhumF+nOdQ6SNm/13WQEN+0I
uNAY2qA4XwWCp8h6wsJdaNNciCU9VZ0lyRkR3kftvVWeAOdg+eV84OKnp3NlhhHG
XbY8XReYQ+BPDYr8jUyuCVKjDnsMsgxqQG8bEMzLa4pKQFcnnIfkEKZ5vosYrrTr
zz95+2xiASsfGztFwBGfqgeECjfueKsZGx+srBGnL4RC7Sr0SRl3Wuqewyy38d6M
51/KbVttT3S1ukxE6xCV3a/F0HDJbn+ragMTkcmSuYwC+osDYB1XdY3+7xdPJUpD
M73vUeD5crppeoOxkJkHrvcRHlRT7V8k+JDWUZHDJ6NQq1Pp1sawgnGQpxzYkv3c
Yhj1ZvbfP7aU2hqEeBaE0DUI3yIA4AcWaMogmtC3AU4HXY6vdsSmoUTPtcMfYt1S
66SW3uJhFaxo9O51VWmnD8C9zrL51u/uJQbr46cpcLjoUBObzpZGSRH60f/TLRAi
cxaRtaByvelqy7Z7Xtz6g/myP+6TxW/8A+lPuRBJR8eCOovoS6y91r5SWe+zW4qP
olDQq8ljf1QkhB8CK8kcOC5WYrUlKrdqFjTGsTHfauzBjEM5OovW4KwO+ZknvH01
jGZkw8eMQl/tWs29tuKIE1OPdJzcsHtavg/+JFjxDy/L9Af0bLneQC84lCjy+yDZ
Npjvcex+odlY9uQllvlW1DPFZfaTzm+xtLxJ+lAjydMGLJZ92MQhwOzEK9o042BN
TYgK9egsVbyyROXdiAflcuBlW7dFc8aL9ZOmia6lHCtvDS7F7ZaaONBRTbTNJZTg
aFnegPSnXWeSV4wZL2zaSlRptdf0LSvdM2azFxq2wfOb6eIc8Qf7nbi8Mvab1uNB
DDIkSKGrh2HVWINeBYPHxuxvev604EVVgn+QN1j8dYFWGAzuXC5vdtfTBPz0Wi4v
l9h3Srd/A/h7EyH0jsBkThnK8v2mIn9GPueaadd0hjhLoArm/s3YkzXmkJEZUewq
R1OkkiDmw9iJ/yzhJ/T7TaCbTFcdg6cG87G0RK0tvqoqfDDC+o/S6dMXAEX1PVpD
VCzxz+5BOB3aybcTx4SheETMnwHuXBMxuNg0uGSBij+2HKMYqJ5AViE9+gnpazOu
s7zw/QRWL0h71gF+PKlfd1baIKhOK67L7a+z5cca6MWCXxu27y/sAzlXSNPmDqGs
NJOG40PaA6f7vWCxNEwwsSb5eMDs5YKA4MWMcjznsO0RGrZ14rAaIqMERI63U6y8
1pLeZiR900BpD6/sBb6Az4dzLQB/QONH9c9lsY74cIGW4KeGrC95MBeVnNs9jsIw
1D7WDOAGmE5paobOwU0EZJ6F4QEQAKlIB+4+h8XQSSUlZ+thBPhjCqq88CkXmcQu
GCOn2u7FAqgB9ji8IKpVBFPgucpYUHWaGOKcO3yjQkLPbrOzDBRcRsLl44IB8C3w
c45WAtsBho1F/PFq2zbbNqPaPc0axqkwbPkrG0Lj9Uah5TS8LqA3bC2pLWhz4ZqU
X/RH0CRetejPtKedRxhTkXitb/U/25/i1oEem7PS+c/dbCj+V75hTxZhNHHRFiyJ
iqf65ox5fDPJjeCRfQkpl5dNQ3vI44tAW4l/UAig3/1jPtmn33WmeBGBptIN1x16
8ng0WDM5xVSS37BOSO9LxtZKsh50xhcomUdkRVRSXDMuDFFgdjJJH8vKihmfugiU
bdVPbRtGX4l7jyxSlLWVxWdVVD23aGEtSXaCr+fT5qUhg2YbSr9oilF6/e5fuaDM
9YU4RszqZ+QSeOvELdW+gFbgqLZoKFs5lT0t4n9POdRQEqj5wMZnRef/4gkwO9vm
8e0dE7I2rCGfF9MwMrRRiUtlSKA8PaXX7ZsOtPxgUWo3cGAHxo/WfOmggV8CuEH/
wpJ1jlpmWsZPQV2As4XUkBjMQIIyzLcOGLRvbqjoYApjeEMWkEPCK4kD2DuM3nTI
zC8inRVg7iA9co/pGpalPo2U/Y6znHcAU1a9a1jpFy2CvVg6RTyadFKXxMk/muY9
RAi4mUGNABEBAAHCw4QEGAEKAA8FAmSeheEFCQAAAAACGy4CKQkQ1MHxg8t7nObB
XSAEGQEKAAYFAmSeheEACgkQJ2z+wfFjNaFIww/8CiGmQEGGSGCG4Kmz/FQbVXjD
VmV2u2A1RzkYYOfbqDZrGqe8uPdyc8QlJP+YDOtMkINefnws5Qe9gCnGnfs5vIMt
ZJIG1op8WdaADTIU7XeYZmyG1S1ceQP3htac5m4To1k5u83NDOqh1kwMxCku9UtT
zEnmF5EK+X0omFiskIHQH3q9exIg6mD7nTWBWmqs2y8NeuRSZexNKHq9Brcf9SSp
PvYC3Vl1e3NHqrBsNLYTGWnnvere2FnCRNd3mmf/iUZEzuYoVe3GyXuLoRYPwj6F
UKLOCuSCLGo1WkJrPU/lF+7Z0++M+G7H5C+Y7ym3FRhi1MjNQohdDf08awsH99jw
9In4MpwXCBqq7ElLH4eZaGMAdP6hcYaFJ0fPL/z5rfo9PntISsyuZmgXJTB8SDNB
C6p4g41xlzlCsLSHWLl0fv1SyY3eY21j1cSxMuShglBB8l0UH9RBzPCPNSlFKkR0
1EN+XwV+4xggu6XDLiWnRW4a29M6jKl0YWypdX8R3zXdQiWQ/2rS+0mU6jCq+op4
E6/8+OuHcIZRDHP7ErskleQTpt3d79yhc2zqUFAQqUXYEz5zcRVFvmc8zjchQXyP
ywCEiOBu4xnCQYUOGbQNQwYEQR5M+vjeJb8mqPa/jM3Se+mqROVaVyPOG03F7cUp
0zmE0vMsJ6weLCGclEVmRRAAnLw2eqt/SJLyIu+Ela8KwoMiY7gBHmhnj2Q/aY0a
6fwtD8/JeZ4CptVueh08vTc0xzzAvegQ4dJ/UMZ3yrufSdUhcbyN9X31AM9c3+wY
x9ttH5LdpiCWw9BWJeMObkscmPMA00hyBl+mHYzcmJswW1ESxyfmkX36uGQmEKdE
M9ELGuolEsa0MRvSNTHDpJJJjSWgXOmXhii0pPcIPXPuInIQhgFHr7868I+LUdOF
ucjFeM9SZ5mMuxg+rV7Bk+zHvyN2vFBz4y14ZOJVFaswYdXb8Mliz8iOe7qLQOWx
fRegtBpYEGn/rrX9r2JY0syiIqQ9knCU5FGtDOmXm+uWtq8YrhX8R3hjEQFkxvIr
ggn2lv4MQ/BDqZQH0GkrECA/iAZvWqNJPX7e3QXmO/BjJk0sPBquRQQEUUW8ShaK
nj9jSLzhYb/hwPyHQ3EmvqE+f4FI3XP8PSwfF8q/9c1HYla+FzeEdUScJ5ia0ymX
wpZEUSewpGw9bO6lVvtB8NEDtx2Sc884GSKqscX7RsRjqeX2mD4zKxDADGvMj1z6
HMMOlAZQkMPstn4ceCmm+rgJ+9pqNyx/sRAfG+OL2ocyKXAoQiofxAgtwgmYNm9w
AQkfl7F58+7ixHAy+b97M4vV3BlHh1TQf5vqzSrqGZceZRHfx2XtH3COtyVYBVcX
hjc=
=bKoy
-----END PGP PUBLIC KEY BLOCK-----
`
