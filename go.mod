module gitlab.com/sdmanager/licchecker

go 1.21

require (
	github.com/ProtonMail/gopenpgp/v2 v2.7.4
	github.com/panta/machineid v1.0.2
)

require (
	github.com/ProtonMail/go-crypto v1.0.0 // indirect
	github.com/ProtonMail/go-mime v0.0.0-20230322103455-7d82a3887f2f // indirect
	github.com/cloudflare/circl v1.3.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
